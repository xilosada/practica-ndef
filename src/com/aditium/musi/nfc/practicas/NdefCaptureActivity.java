package com.aditium.musi.nfc.practicas;

import java.util.ArrayList;

import android.app.ListActivity;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.content.Loader;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.os.FileObserver;
import android.os.Parcelable;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.aditium.musi.nfc.practicas.utils.NdefFileOperationTask;
import com.aditium.musi.nfc.practicas.utils.NdefFilesObserver;
import com.aditium.musi.nfc.practicas.utils.NdefFilesObserver.NdefFileListener;
import com.aditium.musi.nfc.practicas.utils.NdefMessageLoader;

public class NdefCaptureActivity extends ListActivity implements ActionMode.Callback, 
LoaderCallbacks<ArrayList<NdefFile>>, NdefFileListener{ 
	
	private NfcAdapter mAdapter;
	private PendingIntent pendingIntent;
	private String[][] techListsArray;
	NdefMessageAdapter adapter;
	ArrayList<NdefFile> ndeflist= new ArrayList<NdefFile>();
	IntentFilter[] intentFiltersArray;
	private ActionMode mActionMode;
	private int mSelectedIndex;
	private FileObserver mFileObserver;
	
	@Override
    public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		uiBinds();
		nfcConfig();
		getLoaderManager().initLoader(0, null, this);
	}
	
	public void uiBinds(){
		adapter = new NdefMessageAdapter(this, R.layout.row, ndeflist);
		setListAdapter(adapter);
	}
	
	private void nfcConfig() {
		mAdapter = NfcAdapter.getDefaultAdapter(this);
		pendingIntent = PendingIntent.getActivity(
				this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
		try {
			ndef.addDataType("*/*");
		} catch (MalformedMimeTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    intentFiltersArray = new IntentFilter[] {ndef, };
		techListsArray = new String[][]{new String[]{Ndef.class.getName()}};		
	}

	@Override
    public void onResume() {
		super.onResume();
		mAdapter.enableForegroundDispatch(this, pendingIntent, null, techListsArray);
		startObservingFiles();
	}
	
	@Override
    public void onPause() {
		super.onPause();
		mAdapter.disableForegroundDispatch(this);
		stopObservingFiles();
	}
	
	@Override
    public void onNewIntent(Intent intent) {
		NdefMessage[] msgs = null;
        try {
  			Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			if (rawMsgs != null) {
			    msgs = new NdefMessage[rawMsgs.length];
			    for (int i = 0; i < rawMsgs.length; i++) {
			        msgs[i] = (NdefMessage) rawMsgs[i];
			    }
			}
			processNdefMessageArray(msgs);
		} catch (Exception e) {
			e.printStackTrace();
		}	    
	}

	private void processNdefMessageArray(NdefMessage... msgs) {
		// TODO
		for(NdefMessage msg : msgs){
			new NdefFile(msg, null, null);
     	   byte[] data = msg.toByteArray();
     	   String name =String.valueOf(System.currentTimeMillis());
     	   new NdefFileOperationTask(this, name, data).execute();
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
        mSelectedIndex = position;
		mActionMode = startActionMode(this);
		v.setSelected(true);
	}

	@Override
	public boolean onActionItemClicked(ActionMode actionMode, MenuItem item) {
		mActionMode.finish();

		   switch (item.getItemId()) {
	           case R.id.action_delete:
	        	new NdefFileOperationTask(this, ndeflist.get(mSelectedIndex).getFilename(), null).execute();
	            return true;
	           case R.id.action_share:
	        	Intent intent = new Intent(this, TapActivity.class);
	    		intent.putExtra(TapActivity.EXTRA_NDEF_MESSAGE, ndeflist.get(mSelectedIndex).getNdefMessage());
	    		startActivity(intent);
	            return true;
	           default:
               return false;
       }
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.activity_main_actions, menu);
        return true;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		mActionMode = null;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public Loader<ArrayList<NdefFile>> onCreateLoader(int id, Bundle args) {
		return new NdefMessageLoader(this);
	}

	@Override
	public void onLoadFinished(Loader<ArrayList<NdefFile>> loader,
			ArrayList<NdefFile> ndefMessages) {
		ndeflist.clear();
		ndeflist.addAll(ndefMessages);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onLoaderReset(Loader<ArrayList<NdefFile>> arg0) {
	}

	public void startObservingFiles() {
		if(mFileObserver==null){
			mFileObserver = new NdefFilesObserver(this);
		}
		mFileObserver.startWatching();
	}
	public void stopObservingFiles() {
		if(mFileObserver!=null){
			mFileObserver.stopWatching();
		}
	}

	@Override
	public void onFileChange() {
		getLoaderManager().restartLoader(0, null, this);		
	}
}