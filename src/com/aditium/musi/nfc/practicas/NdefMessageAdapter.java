package com.aditium.musi.nfc.practicas;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.content.Context;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NdefMessageAdapter extends ArrayAdapter<NdefFile> {

    private ArrayList<NdefFile> items;
    private Context context;

    
    public NdefMessageAdapter(Context context, int textViewResourceId, ArrayList<NdefFile> items) {
            super(context, textViewResourceId, items);
            this.items = items;
            this.context = context;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                
                v = vi.inflate(R.layout.row, null);
            }
            NdefFile ndefFile =  items.get(position);
            NdefMessage ndefMessage = ndefFile.getNdefMessage();
            for(NdefRecord record: ndefMessage.getRecords()){
                TextView tt = (TextView) v.findViewById(R.id.toptext);
                TextView bt = (TextView) v.findViewById(R.id.bottomtext);
                TextView nt = (TextView) v.findViewById(R.id.filename);
                TextView ct = (TextView) v.findViewById(R.id.creationTime);
                TextView st = (TextView) v.findViewById(R.id.ndefsize);
                ImageView ico = (ImageView) v.findViewById(R.id.icon);
                if (record.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
                    byte type = record.getType()[0];
                    if (type == NdefRecord.RTD_URI[0]) {
                          try {
							tt.setText(new String(record.getPayload(),"UTF-8"));
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}    
                          bt.setText("TNF_WELL_KNOWN: RTD URI");
                          ico.setImageResource(R.drawable.uri_icon);
                    }
                    else if(type == NdefRecord.RTD_TEXT[0]) {
                          try {
							tt.setText(new String(record.getPayload(),"UTF-8"));
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}    
                          bt.setText("TNF_WELL_KNOWN: RTD TEXT");
                          ico.setImageResource(R.drawable.text_icon);
                    }
                    else{
                        try {
							tt.setText(new String(record.getPayload(),"UTF-8"));
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}    
                          bt.setText("TNF_WELL_KNOWN");
                          ico.setImageResource(R.drawable.ic_launcher);
                    }
                } else if (record.getTnf() == NdefRecord.TNF_MIME_MEDIA) {
                    try {
						tt.setText(new String(record.getType(),"UTF-8"));
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}                      
                    bt.setText("TNF_MIME_MEDIA: ");
                    ico.setImageResource(R.drawable.mime_icon);
                }
                else if (record.getTnf() == NdefRecord.TNF_EXTERNAL_TYPE){
                    tt.setText("EXTERNAL ");                            
                      try {
						bt.setText(new String(record.getPayload(),"UTF-8"));
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}    	                        ico.setImageResource(R.drawable.other_icon);
                }else if(record.getTnf() == NdefRecord.TNF_ABSOLUTE_URI){
                    tt.setText("ABSOLUTE_URI ");                            
                    bt.setText("NDEF ");
                    ico.setImageResource(R.drawable.other_icon);
                }else if(record.getTnf() == NdefRecord.TNF_EMPTY){
                    tt.setText("EMPTY ");                            
                    bt.setText("NDEF ");
                    ico.setImageResource(R.drawable.other_icon);
                }
                else{
                    tt.setText("OTRO ");                            
                    bt.setText("NDEF ");
                    ico.setImageResource(R.drawable.other_icon);
                }
                nt.setText(ndefFile.getFilename());
                ct.setText(ndefFile.getCreationTime().toString());
                st.setText(ndefMessage.getByteArrayLength()+" bytes" );
            }
            return v;
    }

	

}