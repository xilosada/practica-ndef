package com.aditium.musi.nfc.practicas;


import java.util.Date;

import android.nfc.NdefMessage;

public class NdefFile {
	private NdefMessage ndefMessage;
	private String filename;
	private Date creationTime;
	
	public NdefFile(NdefMessage ndefMessage, String filename, Date creationTime){
		this.ndefMessage = ndefMessage;
		this.filename = filename;
		this.creationTime = creationTime;
	}
	public NdefMessage getNdefMessage() {
		return ndefMessage;
	}
	public void setNdefMessage(NdefMessage ndefMessage) {
		this.ndefMessage = ndefMessage;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
}
