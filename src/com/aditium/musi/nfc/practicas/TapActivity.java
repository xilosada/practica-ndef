package com.aditium.musi.nfc.practicas;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class TapActivity extends Activity implements CreateNdefMessageCallback, OnNdefPushCompleteCallback  {
	
    private NfcAdapter mAdapter;
	private PendingIntent pendingIntent;
	private IntentFilter[] intentFiltersArray = {
							new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED),
							new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED)
							};
	private String[][] techListsArray= new String[][]{
							new String[]{
									Ndef.class.getName()
									},
							new String[]{
									NdefFormatable.class.getName()
									}
							};
	private NdefMessage msg = null;
    private static final int MESSAGE_SENT = 1;
    public static final String EXTRA_NDEF_MESSAGE = "EXTRA_NDEF_MESSAGE";
    public static Context mContext;

	@Override
    public void onCreate(Bundle icicle){
	super.onCreate(icicle);
	
    setContentView(R.layout.share);
	mContext =  getApplicationContext();
    mAdapter = NfcAdapter.getDefaultAdapter(this);
    mAdapter.setNdefPushMessageCallback(this, this);
    mAdapter.setOnNdefPushCompleteCallback(this,this);
	
    msg = getIntent().getParcelableExtra(EXTRA_NDEF_MESSAGE);
    
    //TODO
	pendingIntent = PendingIntent.getActivity(
			this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//TODO
		mAdapter.disableForegroundDispatch(this);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		// TODO
		mAdapter.enableForegroundDispatch(this, pendingIntent, intentFiltersArray, techListsArray);
		}
	
	@Override
    public void onNewIntent(Intent intent) {
		// TODO
		if (intent.getAction().equals(NfcAdapter.ACTION_NDEF_DISCOVERED)
			|intent.getAction().equals(NfcAdapter.ACTION_TECH_DISCOVERED)){
	        final Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
	        String str = null;
			try {
				str = new AsyncTask<Tag,Void,String>() {
	
					@Override
					protected String doInBackground(Tag... tag) {
						// TODO 
				        try {
				        	
							for(String tech : tag[0].getTechList()){
					            Log.d("TECH",tech);
								if ( tech.equals(NdefFormatable.class.getName()) ){
						        	NdefFormatable ndefTag = NdefFormatable.get(tag[0]);
						            ndefTag.connect();
						        	ndefTag.format(msg);
						            ndefTag.close();
						            break;
								}
								else if (tech.equals(Ndef.class.getName())){
						            Ndef ndefTag = Ndef.get(tag[0]);
						            if (ndefTag.isWritable()){
							            ndefTag.connect();
							        	ndefTag.writeNdefMessage(msg);
						            	ndefTag.close();
						            }
						            else{
							            return "Tag Frozen";
						            	
						            }
						            break;
								}
							}
						} catch (IOException e) {
							// TODO 
							e.printStackTrace();
				            return "IOException";
	
						} catch (FormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
				            return "Formato Ndef no valido";
						}
				        return "Tag Escrito correctamente";
					}
					
				}.execute(tag).get();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ExecutionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Toast.makeText(this, str, Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public NdefMessage createNdefMessage(NfcEvent event) {
		// TODO Auto-generated method stub
		return msg;
	}

    /**
     * Implementation for the OnNdefPushCompleteCallback interface
     */
    @Override
    public void onNdefPushComplete(NfcEvent event) {
        // A handler is needed to send messages to the activity when this
        // callback occurs, because it happens from a binder thread
        mHandler.obtainMessage(MESSAGE_SENT).sendToTarget();
    }

    /** This handler receives a message from onNdefPushComplete */
    private static final Handler mHandler = new Handler() {
    	
    	
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_SENT:
                Toast.makeText(mContext, "Mensaje enviado!", Toast.LENGTH_LONG).show();
                break;
            }
        }
    };

}
