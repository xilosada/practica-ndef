package com.aditium.musi.nfc.practicas.utils;

import java.io.IOException;


import android.content.Context;
import android.os.AsyncTask;


public class NdefFileOperationTask extends AsyncTask<Void, Void, Boolean>{

	private byte[] data;
	private String filename;
	private Context mContext;

	public NdefFileOperationTask(Context context, String filename, byte[] data){
		this.mContext = context;
		this.data = data;
		this.filename = filename;
	}
	
	@Override
	protected Boolean doInBackground(Void... params) {
 	   try {
 		if(data == null){
 			FileUtils.deleteFile(mContext, filename);

 		}else{
 			FileUtils.writeToFile(mContext, filename,  data);
 		   return true;
 		}
	} catch (IOException e) {
	}
	   return false;
	}

}
