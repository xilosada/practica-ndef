package com.aditium.musi.nfc.practicas.utils;

import android.os.Environment;
import android.os.FileObserver;

public class NdefFilesObserver extends FileObserver {
	private final static String PATH = Environment.getExternalStoragePublicDirectory("NDEF").getAbsolutePath();
	private final static int FLAGS = MODIFY |  MOVED_FROM | MOVED_TO | DELETE | CREATE
            | DELETE_SELF | MOVE_SELF; 
	
	public interface NdefFileListener{
		void onFileChange();
	}
	private NdefFileListener mListener;
	public NdefFilesObserver(NdefFileListener listener) {
		super(PATH,FLAGS);
		mListener = listener;
	}

	@Override
	public void onEvent(int event, String path) {
		mListener.onFileChange();
    }
}
