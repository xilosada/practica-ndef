package com.aditium.musi.nfc.practicas.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;

public class FileUtils {

	private final static String FILE_EXTENSION = ".ndef";
	private final static String PARENT_DIR_NAME = "NDEF";

	private static File getParentDir(){
		File parentDir =  Environment.getExternalStoragePublicDirectory(PARENT_DIR_NAME);
		parentDir.mkdirs();
		return parentDir;
	}
	
	public static byte[] readFromFile(File file) throws IOException {
		int size = (int) file.length();
	    byte[] bytes = new byte[size];
        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
        buf.read(bytes, 0, bytes.length);
        buf.close();
	    return bytes;
	}
	
	public static List<File> getListFiles(Context context) {
	    ArrayList<File> inFiles = new ArrayList<File>();
	    File[] files = getParentDir().listFiles();
	    for (File file : files) {
            if(file.getName().endsWith(FILE_EXTENSION)){
                inFiles.add(file);
            }
	    }
	    return inFiles;
	}
	
	public static void writeToFile(Context context, String filename,byte[] data) throws IOException {
		String path =  getParentDir().getAbsolutePath();
		File ndefFile = new File(path+"/"+filename+FILE_EXTENSION);
    	FileOutputStream buf = new FileOutputStream(ndefFile);
        buf.write(data);
        buf.close();
        MediaScannerConnection.scanFile(context, new String[]{ndefFile.getAbsolutePath()}, null, null);
	}

	public static void deleteFile(Context context, String filename) throws IOException {
		String path =  getParentDir().getAbsolutePath();
		File ndefFile = new File(path+"/"+filename);
    	ndefFile.delete();
        MediaScannerConnection.scanFile(context, new String[]{ndefFile.getAbsolutePath()}, null, null);
	}
	
}
