package com.aditium.musi.nfc.practicas.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aditium.musi.nfc.practicas.NdefFile;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.nfc.FormatException;
import android.nfc.NdefMessage;

public class NdefMessageLoader extends AsyncTaskLoader<ArrayList<NdefFile>>{

	private ArrayList<NdefFile> mNdefFiles;
	private Context mContext;

	public NdefMessageLoader(Context context) {
		super(context);
		mContext = context;
    }

	@Override
	public ArrayList<NdefFile> loadInBackground() {
		mNdefFiles = new ArrayList<NdefFile>();
		List<File> files = FileUtils.getListFiles(mContext);
		for(File file: files){
			try {
				NdefFile ndefFile = new NdefFile(new NdefMessage(FileUtils.readFromFile(file)),
						file.getName(),
						new Date(file.lastModified()));
				mNdefFiles.add(ndefFile);
			} catch (FormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return mNdefFiles;
		
	}
	  @Override
	  protected void onStartLoading() {
	    if (mNdefFiles != null) {
	      deliverResult(mNdefFiles);
	    }

	    if (takeContentChanged() || mNdefFiles == null) {
	      forceLoad();
	    }
	  }
}