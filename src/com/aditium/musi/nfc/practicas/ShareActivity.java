package com.aditium.musi.nfc.practicas;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.aditium.musi.nfc.practicas.utils.FileUtils;
import com.aditium.musi.nfc.practicas.utils.NdefFileOperationTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.TextView;
import android.widget.Toast;

public class ShareActivity extends Activity {

	private TextView tvResult;

	@Override
	protected void onCreate(Bundle iCicle){
		super.onCreate(iCicle);
		Intent intent = getIntent();
	    String action = intent.getAction();
	    String type = intent.getType();
	    setContentView(R.layout.share);
	    uiBinds();
	    if (Intent.ACTION_SEND.equals(action) || Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
	        if (type.startsWith("text/")) {
	            handleSendText(intent); // Handle text being sent
	        } else if (intent.getStringExtra(Intent.EXTRA_STREAM)!=null||
	        		intent.getParcelableExtra(Intent.EXTRA_STREAM)!=null){
	            handleSendMedia (intent); // Handle single image being sent
	        }
	    }
	}

	private void uiBinds() {
		tvResult = (TextView) findViewById(R.id.textView1);
		tvResult.setText("");
	}

	private void handleSendText(Intent intent) {
    	String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
    	NdefRecord record = NdefRecord.createMime(intent.getType(), sharedText.getBytes());
        saveNdefs(record);
	}
	
	private void handleSendMedia(Intent intent) {
        ArrayList<Uri> imageUris = new ArrayList<Uri>();
        if(Intent.ACTION_SEND.equals(intent.getAction())){
            imageUris.add((Uri)intent.getParcelableExtra(Intent.EXTRA_STREAM));
        }else if(Intent.ACTION_SEND_MULTIPLE.equals(intent.getAction())){
        	imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        }
        NdefRecord[] records= new NdefRecord[imageUris.size()];
        int i=0;
        for(Uri uri :imageUris){
        	byte[] data = getBytesFromUri(uri);
        	if(data != null){
            	records[i] = NdefRecord.createMime(intent.getType(), data);
        	}else{
    			Toast.makeText(this, "No se ha encontrado el fichero", Toast.LENGTH_SHORT).show();
        	}
        	i++;
        }
        saveNdefs(records);
    }
			
	void saveNdefs(NdefRecord...  records){
		NdefMessage msg = new NdefMessage(records);
		byte[] data = msg.toByteArray();
		String name =String.valueOf(System.currentTimeMillis());
		try {
			if(new NdefFileOperationTask(this, name, data).execute().get()){
				tvResult.setText("Fichero salvado");

				Toast.makeText(this, "Fichero salvado", Toast.LENGTH_SHORT).show();
			}else{
				tvResult.setText("Error guardando el fichero");

				Toast.makeText(this, "Error Guardando el fichero", Toast.LENGTH_SHORT).show();
			}
		} catch (InterruptedException e) {
			Toast.makeText(this, "InterruptedException", Toast.LENGTH_SHORT).show();
			tvResult.setText("Error Guardando el fichero");
			e.printStackTrace();
		} catch (ExecutionException e) {
			Toast.makeText(this, "Execution exception", Toast.LENGTH_SHORT).show();
			tvResult.setText("Error Guardando el fichero");
		}
	}
		
	byte[] getBytesFromUri(Uri uri){
		try{
	    	File file =  new File(getRealPathFromURI(this,uri));
	    	if(file!=null){
				byte[] data = null;
					data = FileUtils.readFromFile(file);
					return data;
	    	}	    	
	    }catch (Exception e){
	    	Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
		return null;
    }
	
    public String getRealPathFromURI(Context context, Uri contentUri) {
    	Cursor cursor = null;
    	try { 
    		String[] proj = { MediaStore.Images.Media.DATA };
    		cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
    		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
    		cursor.moveToFirst();
    		return cursor.getString(column_index);
    	} finally {
    		if (cursor != null) {
    			cursor.close();
	    	}
	    }
    }
}
